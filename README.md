# AreUSmart #

Exercise is a maven project.

To run the program please ```use pl.parser.nbp.MainClass```.
Project uses thirdparty libs, so in order to run program from command line you need to add it to your classpath.

More details about thirdparties may be found in pom.xml.

You can find also various tests inside the project.
ExerciseExampleIntegrationTest shows basic usage and prooves it works on provided test case (in exercise description).
