package pl.parser.nbp;


import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.time.LocalDate;
import java.util.Currency;

import static org.assertj.core.api.Assertions.assertThat;

public class CurrencyOperationsTest {


    @Test
    public void should_create_fixed_currency_period_operator(){
        CurrencyOperator operator = new CurrencyOperations()
                .forCurrency(Currency.getInstance("PLN"))
                .withinTimeFrame(LocalDate.now(), LocalDate.now().plusMonths(1))
                .createOperator();

        assertThat(operator).isNotNull().isInstanceOf(FixedCurrencyPeriodOperator.class);
    }
}
