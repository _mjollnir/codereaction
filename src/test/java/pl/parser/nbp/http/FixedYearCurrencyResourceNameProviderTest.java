package pl.parser.nbp.http;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.assertj.core.api.Assertions;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.time.LocalDate;
import java.time.Year;
import java.util.Map;

import static junitparams.JUnitParamsRunner.$;

@RunWith(JUnitParamsRunner.class)
public class FixedYearCurrencyResourceNameProviderTest {

    private static Map<LocalDate, String> mapping;
    private static Year year;

    @BeforeClass
    public static void beforeAll(){
        year = Year.of(2007);
        mapping = new NBPCurrencyFilesMappingDownloader().downloadCurrencyFilesMappingForYear(year);
    }

    @Test
    @TestCaseName("nbpCurrencyResourcePath [{index}]")
    @Parameters(method = "nbpCurrencyResourcePathForFixedYear")
    public void should_build_proper_resource_path(LocalDate resourcePublicationDate, String resourceFilePath){
        FixedYearCurrencyResourceNameProvider provider = new FixedYearCurrencyResourceNameProvider(mapping,year);

        Assertions.assertThat(provider.getCurrencyResourceNameForDate(resourcePublicationDate))
                .isPresent()
                .hasValue(resourceFilePath);
    }

    public Object nbpCurrencyResourcePathForFixedYear(){
        return $(
                $(LocalDate.parse("2007-04-13"),"c073z070413")
        );
    }

}
