package pl.parser.nbp.http;


import org.assertj.core.api.Assertions;
import org.junit.Test;

import java.time.LocalDate;
import java.time.Year;
import java.util.Map;

public class NBPCurrencyFilesMappingDownloaderIntegrationTest {

    @Test
    public void should_download_publications_map_for_supported_historical_year(){
        NBPCurrencyFilesMappingDownloader downloader = new NBPCurrencyFilesMappingDownloader();

        Map<LocalDate, String> publicationsMap = downloader.downloadCurrencyFilesMappingForYear(Year.now().minusYears(3));

        Assertions.assertThat(publicationsMap)
                .isNotEmpty()
                .containsKeys(LocalDate.parse("2013-12-27"),LocalDate.parse("2013-06-21"),LocalDate.parse("2013-02-28"))
                .doesNotContainKeys(LocalDate.parse("2013-03-02"));

    }

    @Test
    public void should_download_publications_map_for_supported_current_year(){
        NBPCurrencyFilesMappingDownloader downloader = new NBPCurrencyFilesMappingDownloader();

        Map<LocalDate, String> publicationsMap = downloader.downloadCurrencyFilesMappingForYear(Year.now());

        Assertions.assertThat(publicationsMap)
                .isNotEmpty()
                .containsKeys(LocalDate.parse("2016-01-04"),LocalDate.parse("2016-02-29"))
                .doesNotContainKeys(LocalDate.parse("2016-03-05"));

    }

    @Test
    public void should_fail_for_year_before_2002(){
        NBPCurrencyFilesMappingDownloader downloader = new NBPCurrencyFilesMappingDownloader();

        Assertions.assertThatThrownBy(() -> {
            downloader.downloadCurrencyFilesMappingForYear(Year.of(2001));
        })
       .isInstanceOf(PublicationDateOutOfSupportedRangeException.class)
       .hasMessage("Not supported year:2001")
       .hasNoCause();
    }

    @Test
    public void should_fail_for_future_year(){
        NBPCurrencyFilesMappingDownloader downloader = new NBPCurrencyFilesMappingDownloader();

        Assertions.assertThatThrownBy(() -> {
            downloader.downloadCurrencyFilesMappingForYear(Year.of(2017));
        })
        .isInstanceOf(PublicationDateOutOfSupportedRangeException.class)
        .hasMessage("Not supported year:2017")
        .hasNoCause();
    }

}
