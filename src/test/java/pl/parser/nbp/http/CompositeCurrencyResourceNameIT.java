package pl.parser.nbp.http;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.assertj.core.api.Assertions;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.time.LocalDate;
import java.time.Year;

import static junitparams.JUnitParamsRunner.$;

@RunWith(JUnitParamsRunner.class)
public class CompositeCurrencyResourceNameIT {


    private static NBPCurrencyResourceNameProvider provider;

    @BeforeClass
    public static void beforeAll(){
        provider = new CompositeCurrencyResourceNameProvider.Builder()
                .addSupportForYear(2013)
                .addSupportForYear(2014)
                .addSupportForYear(2015)
                .addSupportForYear(2016)
        .build();
    }

    @Test
    @TestCaseName("nbpCurrencyResourcePath [{index}]")
    @Parameters(method = "nbpCurrencyResourcePathForFixedYear")
    public void should_build_proper_resource_path(LocalDate resourcePublicationDate, String resourceFilePath){
        Assertions.assertThat(provider.getCurrencyResourceNameForDate(resourcePublicationDate))
                .isPresent()
                .hasValue(resourceFilePath);
    }

    public Object nbpCurrencyResourcePathForFixedYear(){
        return $(
                $(LocalDate.parse("2015-09-18"),"c182z150918"),
                $(LocalDate.parse("2013-12-27"),"c249z131227"),
                $(LocalDate.parse("2014-01-02"),"c001z140102"),
                $(LocalDate.parse("2016-03-09"),"c047z160309")
        );
    }

}
