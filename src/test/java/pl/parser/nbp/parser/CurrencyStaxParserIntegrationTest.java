package pl.parser.nbp.parser;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import pl.parser.nbp.CurrencyDailyInfo;

import javax.xml.stream.XMLStreamException;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.util.Currency;
import java.util.Optional;

/**
 * Created by tomasz.juszczyk on 2016-03-28.
 */
public class CurrencyStaxParserIntegrationTest {

    @Test
    public void should_parse_basic_test_case() throws XMLStreamException {
        Reader source = new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("c073z070413.xml"), Charset.forName("ISO-8859-2"));

        CurrencyParser parser = new CurrencyStaxParser();

        Currency currency = Currency.getInstance("CHF");
        Optional<CurrencyDailyInfo> data = parser.parseInput(source, currency);

        Assertions.assertThat(data).isPresent();
        Assertions.assertThat(data.get().getCurrency()).isEqualTo(currency);
        Assertions.assertThat(data.get().getMultiplier()).isEqualTo(1);
        Assertions.assertThat(data.get().getPublicationDate()).isEqualTo("2007-04-13");
        Assertions.assertThat(data.get().getBuyingPrice()).isEqualTo(new BigDecimal("2.3163"));
        Assertions.assertThat(data.get().getSellingPrice()).isEqualTo(new BigDecimal("2.3631"));
    }
}
