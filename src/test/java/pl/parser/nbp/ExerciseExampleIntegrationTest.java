package pl.parser.nbp;


import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Currency;

import static org.assertj.core.api.Assertions.assertThat;

public class ExerciseExampleIntegrationTest {

    @Test
    public void should_handle_test_case_given_as_an_example(){
        CurrencyOperator operator = new CurrencyOperations()
                .forCurrency(Currency.getInstance("EUR"))
                .withinTimeFrame(LocalDate.parse("2013-01-28"), LocalDate.parse("2013-01-31"))
                .createOperator();

        assertThat(operator.averageBuyingPrice()).isEqualTo(new BigDecimal("4.1505"));
        assertThat(operator.standardDeviationSellingPrice()).isEqualTo(new BigDecimal("0.0125"));
    }

    @Test
    public void current_year(){
        CurrencyOperator operator = new CurrencyOperations()
                .forCurrency(Currency.getInstance("EUR"))
                .withinTimeFrame(LocalDate.parse("2016-01-01"), LocalDate.parse("2016-03-31"))
                .createOperator();

        assertThat(operator.averageBuyingPrice()).isNotNull();
        assertThat(operator.standardDeviationSellingPrice()).isNotNull();
    }

    @Test
    public void long_period(){
        CurrencyOperator operator = new CurrencyOperations()
                .forCurrency(Currency.getInstance("EUR"))
                .withinTimeFrame(LocalDate.parse("2009-01-01"), LocalDate.parse("2016-01-01"))
                .createOperator();

        BigDecimal average = operator.averageBuyingPrice();
        assertThat(average).isNotNull();
        BigDecimal std = operator.standardDeviationSellingPrice();
        assertThat(std).isNotNull();

        System.out.println(String.format("AVG=%s , STD=%s",average,std));
    }
}
