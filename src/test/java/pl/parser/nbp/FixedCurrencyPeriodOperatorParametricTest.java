package pl.parser.nbp;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import junitparams.naming.TestCaseName;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Currency;
import java.util.stream.Stream;

import static junitparams.JUnitParamsRunner.$;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(JUnitParamsRunner.class)
public class FixedCurrencyPeriodOperatorParametricTest {


    @Mock
    CurrenciesDataSet dataSet;

    LocalDate from = LocalDate.now();

    LocalDate to = LocalDate.now();

    FixedCurrencyPeriodOperator operator;
    private Currency currency = Currency.getInstance("EUR");

    @Before
    public void setUp(){
        initMocks(this);

        operator = new FixedCurrencyPeriodOperator(currency,from,to,dataSet);
    }

    @Test
    @TestCaseName("averageBuyingPrice [{index}]")
    @Parameters(method = "averageData")
    public void should_count_big_decimal_average(Stream<BigDecimal> numbers, BigDecimal expectedAverage){
        given(dataSet.buyingCurrencies()).willReturn(numbers);

        BigDecimal average = operator.averageBuyingPrice();

        Assertions.assertThat(average).isEqualTo(expectedAverage);
    }

    public Object averageData(){
        return $(
                $(Stream.of(1,2,3,4,5).map(number -> BigDecimal.valueOf(number)), new BigDecimal("3.0000")),
                $(Stream.of(1,2,3,4,5,6).map(number -> BigDecimal.valueOf(number)), new BigDecimal("3.5000")),
                $(Stream.of(1.1111,2.2222,3.3333,4.4444,5.55555,6.6666).map(number -> BigDecimal.valueOf(number)), new BigDecimal("3.8889"))
        );
    }

}
