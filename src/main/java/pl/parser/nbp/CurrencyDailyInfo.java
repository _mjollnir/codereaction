package pl.parser.nbp;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Currency;
import java.util.Locale;
import java.util.Objects;

import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;

/**
 * Created by tomasz.juszczyk on 2016-03-26.
 */
public class CurrencyDailyInfo {
    private final Currency currency;
    private final LocalDate publicationDate;
    private final BigDecimal buyingPrice;
    private final BigDecimal sellingPrice;
    private final int multiplier;

    private CurrencyDailyInfo(Currency currency, LocalDate publicationDate, BigDecimal buyingPrice, BigDecimal sellingPrice, int multiplier) {
        this.currency = currency;
        this.publicationDate = publicationDate;
        this.buyingPrice = buyingPrice;
        this.sellingPrice = sellingPrice;
        this.multiplier = multiplier;
    }

    public Currency getCurrency() {
        return currency;
    }

    public LocalDate getPublicationDate() {
        return publicationDate;
    }

    public BigDecimal getBuyingPrice() {
        return buyingPrice;
    }

    public BigDecimal getSellingPrice() {
        return sellingPrice;
    }

    public int getMultiplier() {
        return multiplier;
    }

    public static class CurrencyDayInfoBuilder {
        private  Currency currency;
        private  LocalDate publicationDate;
        private  BigDecimal buyingPrice;
        private  BigDecimal sellingPrice;
        private  int multiplier=1;

        private DecimalFormat decimalFormat;

        public CurrencyDayInfoBuilder(){
            DecimalFormatSymbols symbols = new DecimalFormatSymbols();
            symbols.setDecimalSeparator(',');
            symbols.setGroupingSeparator(' ');

            decimalFormat = new DecimalFormat("#,####.##", symbols);
            decimalFormat.setParseBigDecimal(true);
        }

        public CurrencyDailyInfo build(){
            requireNonNull(currency);
            requireNonNull(publicationDate);
            requireNonNull(buyingPrice);
            requireNonNull(sellingPrice);

            return new CurrencyDailyInfo(currency,publicationDate,buyingPrice,sellingPrice,multiplier);
        }

        public void withPublicationDate(String localDateString) {
            publicationDate = LocalDate.parse(localDateString, DateTimeFormatter.ISO_LOCAL_DATE);
        }

        public void withSellingPrice(String elementText) throws ParseException {
            sellingPrice = (BigDecimal) decimalFormat.parse(elementText);
        }

        public void withBuyingPrice(String elementText) throws ParseException {
            buyingPrice = (BigDecimal) decimalFormat.parse(elementText);
        }

        public boolean allRequiredDataProvided() {
            return  !isNull(currency) &&
                    !isNull(publicationDate) &&
                    !isNull(buyingPrice) &&
                    !isNull(sellingPrice);
        }

        public void withMultiplier(String elementText) {
            multiplier = Integer.parseInt(elementText);
        }

        public void withCurrency(Currency currency) {
            this.currency=currency;
        }
    }
}
