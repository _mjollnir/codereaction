package pl.parser.nbp;


import java.text.DateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Currency;

import static java.text.MessageFormat.format;

public class MainClass
{
    public static void main( String[] args ) {
        if(args.length<3){
            throw new IllegalArgumentException(format("Wrong number of input parameters: {0}, need {1} parameters!",args.length,3));
        }
        Currency currency = obtainProperCurrency(args[0]);
        LocalDate startOfPeriod = obtainProperDate(args[1]);
        LocalDate endOfPeriod = obtainProperDate(args[2]);

        System.out.println(format("Currency: {0}, Date period: [{1} : {2}]",args));

        CurrencyOperator operator = new CurrencyOperations()
                .forCurrency(currency)
                .withinTimeFrame(startOfPeriod,endOfPeriod)
                .createOperator();

        System.out.println(format("Average buying price: {0,number,#.####}",operator.averageBuyingPrice()));
        System.out.println(format("Selling price standard deviation: {0,number,#.####}",operator.standardDeviationSellingPrice()));

    }

    /*
    For a sake of the exercise only one format supported
     */
    private static LocalDate obtainProperDate(String date) {
        return LocalDate.parse(date, DateTimeFormatter.ISO_LOCAL_DATE);
    }

    private static Currency obtainProperCurrency(String currencyIsoCode) {
        return Currency.getInstance(currencyIsoCode);
    }
}
