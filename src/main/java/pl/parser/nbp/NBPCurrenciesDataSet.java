package pl.parser.nbp;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;


public class NBPCurrenciesDataSet implements CurrenciesDataSet {

    private final Currency currency;
    private final List<CurrencyDailyInfo> dailyDataList;

    public NBPCurrenciesDataSet(Currency currency, List<CurrencyDailyInfo> dailyDataList){
        this.currency = currency;
        this.dailyDataList = dailyDataList;
    }

    @Override
    public Stream<BigDecimal> buyingCurrencies() {
        return dailyDataList.stream().map( CurrencyDailyInfo::getBuyingPrice );
    }

    @Override
    public Stream<BigDecimal> sellingCurrencies() {
        return dailyDataList.stream().map( CurrencyDailyInfo::getSellingPrice );
    }

    public Currency currency(){
        return currency;
    };


}
