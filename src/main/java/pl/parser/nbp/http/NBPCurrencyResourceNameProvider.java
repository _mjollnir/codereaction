package pl.parser.nbp.http;


import java.time.LocalDate;
import java.util.Optional;

public interface NBPCurrencyResourceNameProvider {

    public Optional<String> getCurrencyResourceNameForDate(LocalDate publicationDate);


}
