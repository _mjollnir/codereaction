package pl.parser.nbp.http;


import java.time.format.DateTimeFormatter;

public interface NBPSettings {
    public static final String NBP_HOST = "http://www.nbp.pl";
    public static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyMMdd");
    public static final String CURRENCY_FILES_PREFIX = "c";

}
