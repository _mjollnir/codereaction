package pl.parser.nbp.http;


import java.time.LocalDate;
import java.time.Year;
import java.util.Map;
import java.util.Optional;

public class FixedYearCurrencyResourceNameProvider implements NBPCurrencyResourceNameProvider {

    private final Map<LocalDate, String> resourceNamesMapping;
    private final Year year;

    FixedYearCurrencyResourceNameProvider(Map<LocalDate, String> nameMapping, Year year) {
        this.year = year;
        this.resourceNamesMapping = nameMapping;
    }

    @Override
    public Optional<String> getCurrencyResourceNameForDate(LocalDate publicationDate) {
        Year year = Year.of(publicationDate.getYear());
        if (!this.year.equals(year)) {
            throw new PublicationDateOutOfSupportedRangeException("Year mismatch:" + year);
        }
        return Optional.ofNullable(resourceNamesMapping.get(publicationDate));
    }

    public Year year() {
        return year;
    }
}
