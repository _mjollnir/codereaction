package pl.parser.nbp.http;

import org.apache.commons.io.input.BOMInputStream;
import org.apache.http.client.fluent.Request;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.LocalDate;
import java.time.Year;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static java.lang.String.format;
import static org.apache.commons.io.ByteOrderMark.*;
import static org.apache.commons.io.ByteOrderMark.UTF_8;

public class NBPCurrencyFilesMappingDownloader {
    public Map<LocalDate,String> downloadCurrencyFilesMappingForYear(Year year) {

        if (year.isBefore(Year.of(2002))){
            throw new PublicationDateOutOfSupportedRangeException(format("Not supported year:%s",year));
        }
        if (year.isAfter(Year.now())){
            throw new PublicationDateOutOfSupportedRangeException(format("Not supported year:%s",year));
        }
        Map<LocalDate,String> publicationFilesMap = new HashMap<>();
        String dirFileName = format("/kursy/xml/dir%s.txt", year.equals(Year.now()) ? "" : year.getValue());
        try {
            InputStream responseStream = Request.Get(NBPSettings.NBP_HOST + dirFileName)
                    .execute()
                    .returnContent()
                    .asStream();
            BOMInputStream bomInputStream = new BOMInputStream(responseStream, UTF_16LE, UTF_16BE, UTF_32LE, UTF_32BE, UTF_8);
            BufferedReader reader = new BufferedReader(new InputStreamReader(bomInputStream));
            String line;
            while((line  = reader.readLine()) != null){
                String trimmedLine = line.trim();
                if (trimmedLine.startsWith(NBPSettings.CURRENCY_FILES_PREFIX)){
                    publicationFilesMap.put(createDateFrompublicationFileName(trimmedLine), trimmedLine);
                }
            }
        } catch (IOException e) {
            throw new NBPDataConnectionException("Problem downloading/analyzing NBP dir file"+dirFileName,e);
        }
        return Collections.unmodifiableMap(publicationFilesMap);
    }

    private LocalDate createDateFrompublicationFileName(String fileName) {
        return LocalDate.parse(fileName.substring(fileName.length()-6),NBPSettings.dateFormatter);
    }

}
