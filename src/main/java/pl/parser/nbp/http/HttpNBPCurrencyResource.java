package pl.parser.nbp.http;

import org.apache.http.client.fluent.Request;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Currency;


public class HttpNBPCurrencyResource {

    public String getFileName() {
        return fileName;
    }

    private String fileName;

    public HttpNBPCurrencyResource(String currenciesFileName){
        this.fileName=currenciesFileName;
    };

    public Reader getReader() {
        try {
            return new InputStreamReader(
                    Request.Get(NBPSettings.NBP_HOST+String.format("/kursy/xml/%s.xml",fileName))
                    .execute()
                    .returnContent()
                    .asStream(),"ISO-8859-2");
        } catch (IOException e) {
            throw new NBPDataConnectionException(String.format("CurrencyResource unavailable: %s",fileName),e);
        }
    }
}
