package pl.parser.nbp.http;


public class PublicationDateOutOfSupportedRangeException extends RuntimeException {

    public PublicationDateOutOfSupportedRangeException(String message){
        super(message);
    }
    public PublicationDateOutOfSupportedRangeException(String message, Exception reason){
        super(message,reason);
    }
}
