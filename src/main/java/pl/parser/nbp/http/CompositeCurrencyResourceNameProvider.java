package pl.parser.nbp.http;


import java.time.LocalDate;
import java.time.Year;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class CompositeCurrencyResourceNameProvider implements NBPCurrencyResourceNameProvider{

    private Map<Year,FixedYearCurrencyResourceNameProvider> providersMap = new HashMap<>();

    CompositeCurrencyResourceNameProvider() {

    }

    void add(FixedYearCurrencyResourceNameProvider fixed) {
        providersMap.put(fixed.year(),fixed);
    }

    private void combine(CompositeCurrencyResourceNameProvider composite2) {
        composite2.providersMap.values().forEach((fixed) -> providersMap.put(fixed.year(),fixed));
    }

    @Override
    public Optional<String> getCurrencyResourceNameForDate(LocalDate publicationDate) {
        Year year = Year.of(publicationDate.getYear());
        FixedYearCurrencyResourceNameProvider yearCurrencyResourceNameProvider = providersMap.get(year);
        return yearCurrencyResourceNameProvider==null?
                Optional.empty():
                yearCurrencyResourceNameProvider.getCurrencyResourceNameForDate(publicationDate);
    }

    public static class Builder {
        List<Year> supportedYears = new ArrayList<>();
        private NBPCurrencyFilesMappingDownloader downloader = new NBPCurrencyFilesMappingDownloader();

        public Builder addSupportForYear(Year year){
            supportedYears.add(year);
            return this;
        }
        public Builder addSupportForYear(int year){
            supportedYears.add(Year.of(year));
            return this;
        }

        public NBPCurrencyResourceNameProvider build(){
            CompositeCurrencyResourceNameProvider composite = supportedYears.stream()
                    .parallel()
                    .map((year) -> new FixedYearCurrencyResourceNameProvider(downloader.downloadCurrencyFilesMappingForYear(year), year) )
                    .collect(CompositeCurrencyResourceNameProvider::new,CompositeCurrencyResourceNameProvider::add,CompositeCurrencyResourceNameProvider::combine);
            return composite;
        }
    }

}
