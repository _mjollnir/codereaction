package pl.parser.nbp.http;


public class NBPDataConnectionException extends RuntimeException {

    public NBPDataConnectionException(String message,Exception reason){
        super(message,reason);
    }

}
