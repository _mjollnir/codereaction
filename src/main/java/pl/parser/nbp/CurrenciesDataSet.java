package pl.parser.nbp;


import java.math.BigDecimal;
import java.util.Currency;
import java.util.stream.Stream;

public interface CurrenciesDataSet {

    Stream<BigDecimal> buyingCurrencies();

    Stream<BigDecimal>  sellingCurrencies();

    public Currency currency();

}
