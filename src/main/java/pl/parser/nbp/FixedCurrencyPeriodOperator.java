package pl.parser.nbp;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Currency;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.Stream;

/**
 * Created by tomasz.juszczyk on 2016-03-23.
 */
public class FixedCurrencyPeriodOperator implements CurrencyOperator {

    private final Currency currency;
    private final LocalDate to;
    private final LocalDate from;
    private CurrenciesDataSet dataSet;

    public FixedCurrencyPeriodOperator(Currency currency, LocalDate from, LocalDate to, CurrenciesDataSet dataSet) {
        Objects.requireNonNull(dataSet);
        Objects.requireNonNull(currency);
        Objects.requireNonNull(from);
        Objects.requireNonNull(to);

        this.from=from;
        this.to=to;
        this.currency=currency;
        this.dataSet=dataSet;
    }


    public BigDecimal averageBuyingPrice() {

        return dataSet.buyingCurrencies()
                .collect(CurrenciesAverageComposer::new, CurrenciesAverageComposer::accept, CurrenciesAverageComposer::combine)
                .average();

    }

    public BigDecimal standardDeviationSellingPrice() {
        BigDecimal average = average(dataSet.sellingCurrencies());
        return dataSet.sellingCurrencies()
                .collect(() -> {
                    return new StandardDeviationComposer(average);
                }, StandardDeviationComposer::accept, StandardDeviationComposer::combine
                )
                .standardDeviation();
    }

    private BigDecimal average(Stream<BigDecimal> stream){
        return stream
                .collect(CurrenciesAverageComposer::new, CurrenciesAverageComposer::accept, CurrenciesAverageComposer::combine)
                .average();
    }



    public static class CurrenciesAverageComposer implements Consumer<BigDecimal>{
        private BigDecimal sum;
        private long count;

        public CurrenciesAverageComposer() {
            this.sum=BigDecimal.ZERO;
            this.count=0;
        }

        @Override
        public void accept(BigDecimal bigDecimal) {
            count++;
            sum = sum.add(bigDecimal);
        }

        public void combine(CurrenciesAverageComposer currenciesComposer){
            sum = sum.add(currenciesComposer.sum);
            count+=currenciesComposer.count;
        }

        public BigDecimal average() {
            return sum.divide(BigDecimal.valueOf(count),4,BigDecimal.ROUND_HALF_EVEN);
        }
    }

    public static class StandardDeviationComposer implements Consumer<BigDecimal>{
        private final BigDecimal average;
        private final BigDecimal negatedAverage;
        private BigDecimal sum;
        private long count;

        public StandardDeviationComposer(BigDecimal average) {
            this.average = average;
            this.negatedAverage = average.negate();
            this.sum=BigDecimal.ZERO;
            this.count=0;
        }

        @Override
        public void accept(BigDecimal bigDecimal) {
            count++;
            sum = sum.add(bigDecimal.add(negatedAverage).pow(2));
        }

        public void combine(StandardDeviationComposer currenciesComposer){
            if(!average.equals(currenciesComposer.average)){
                throw new IllegalArgumentException("We can compose only if averages matches");
            }

            sum = sum.add(currenciesComposer.sum);
            count+=currenciesComposer.count;
        }

        /**
         * Initial naive solution
         * @return
         */
        public BigDecimal standardDeviation() {
            double doubleValue = Math.sqrt(sum.divide(BigDecimal.valueOf(count),MathContext.DECIMAL64).doubleValue());
            BigDecimal bigDecimal = new BigDecimal(doubleValue, new MathContext(4, RoundingMode.HALF_EVEN)).setScale(4,BigDecimal.ROUND_HALF_EVEN);
            return bigDecimal;
        }
    }
}
