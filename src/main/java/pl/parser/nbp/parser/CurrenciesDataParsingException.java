package pl.parser.nbp.parser;

/**
 * Created by tomasz.juszczyk on 2016-03-26.
 */
public class CurrenciesDataParsingException extends RuntimeException {
    public CurrenciesDataParsingException(Exception e) {
        super(e);
    }
}
