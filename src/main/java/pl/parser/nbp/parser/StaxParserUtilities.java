package pl.parser.nbp.parser;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 * Created by tomasz.juszczyk on 2016-03-28.
 */
public class StaxParserUtilities {

    public static void forwardToMatchingEndElement(XMLEventReader eventReader, StartElement startElement) throws XMLStreamException {
        XMLEvent event = eventReader.nextEvent();
        while (!event.isEndElement() || !endElementMatchesStartElement(startElement, event.asEndElement())){
            event = eventReader.nextEvent();
        }
    }

    public static boolean endElementMatchesStartElement(StartElement startElement, EndElement endElement) {
        return endElement.getName().getLocalPart().equals(startElement.getName().getLocalPart());
    }

    public static  boolean endElementMatchesStartElement(String startElementLocalName, EndElement endElement) {
        return startElementLocalName.equals(endElement.getName().getLocalPart());
    }
}
