package pl.parser.nbp.parser;


import pl.parser.nbp.CurrencyDailyInfo;

import javax.swing.event.DocumentEvent;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.Reader;
import java.text.ParseException;
import java.util.Currency;
import java.util.Optional;

import static pl.parser.nbp.CurrencyDailyInfo.*;

/*
 Assumption  (for the exercise purposes): I assume XML schema has sequence inside <pozycja> tag (fixed order of subelements)
 */
public class CurrencyStaxParser implements CurrencyParser {

    public static final String DATA_PUBLIKACJI = "data_publikacji";
    public static final String POZYCJA = "pozycja";
    public static final String PRZELICZNIK = "przelicznik";
    public static final String KOD_WALUTY = "kod_waluty";
    public static final String KURS_KUPNA = "kurs_kupna";
    public static final String KURS_SPRZEDAZY = "kurs_sprzedazy";

    @Override
    public Optional<CurrencyDailyInfo> parseInput(Reader reader, Currency currency) {
        XMLInputFactory factory = XMLInputFactory.newInstance();
        CurrencyDayInfoBuilder builder = new CurrencyDayInfoBuilder();
        XMLEventReader eventReader = null;
        boolean currencyInfoNotFound = true;
        try {
            eventReader = factory.createXMLEventReader(reader);
            while(eventReader.hasNext() && currencyInfoNotFound) {
                XMLEvent event = eventReader.nextEvent();
                if(event.getEventType()==XMLEvent.START_ELEMENT ){
                    StartElement startElement = event.asStartElement();
                    String qName = startElement.getName().getLocalPart();
                    if (qName.equalsIgnoreCase(DATA_PUBLIKACJI)){
                        readPublicationDate(eventReader,builder);
                    } else  if (qName.equalsIgnoreCase(POZYCJA)) {
                        currencyInfoNotFound = ! readCurrencyDailyInfoIfMatchesCurrency(eventReader,builder,currency);
                        if(currencyInfoNotFound){
                            StaxParserUtilities.forwardToMatchingEndElement(eventReader,startElement);
                        }
                    }

                }
            }
            if(currencyInfoNotFound || !builder.allRequiredDataProvided()){
                return Optional.empty();
            }

            return Optional.of(builder.build());
        } catch (XMLStreamException | ParseException | NumberFormatException e) {
            throw new CurrenciesDataParsingException(e);
        }

    }


    private boolean readCurrencyDailyInfoIfMatchesCurrency(XMLEventReader eventReader, CurrencyDayInfoBuilder builder, Currency currency) throws XMLStreamException, ParseException {
        XMLEvent event = eventReader.nextEvent();
        boolean currencyMismatch = false;
        while(!isEndOfCurrencyDetails(event) && !currencyMismatch){
            if(event.isStartElement()){
                switch (event.asStartElement().getName().getLocalPart()) {
                    case PRZELICZNIK: readMultiplier(eventReader,builder);
                        break;
                    case KOD_WALUTY: {
                        currencyMismatch =  !isMatchingCurrency(eventReader,currency);
                        if (!currencyMismatch) {
                            builder.withCurrency(currency);
                        }
                    };
                        break;
                    case KURS_KUPNA: readBuyingPrice(eventReader,builder);
                        break;
                    case KURS_SPRZEDAZY: readSellingPrice(eventReader,builder);
                        break;
                }
            }
            event = eventReader.nextEvent();
        }

        return !currencyMismatch;
    }

    private void readSellingPrice(XMLEventReader eventReader, CurrencyDayInfoBuilder builder) throws XMLStreamException, ParseException {
        builder.withSellingPrice(eventReader.getElementText());
    }

    private void readBuyingPrice(XMLEventReader eventReader, CurrencyDayInfoBuilder builder) throws XMLStreamException, ParseException {
        builder.withBuyingPrice(eventReader.getElementText());
    }

    private boolean isMatchingCurrency(XMLEventReader eventReader, Currency currency) throws XMLStreamException {
        String text = eventReader.getElementText();
        return text.equals(currency.getCurrencyCode());
    }

    private void readMultiplier(XMLEventReader eventReader, CurrencyDayInfoBuilder builder) throws XMLStreamException {
        builder.withMultiplier(eventReader.getElementText());
    }

    private boolean isEndOfCurrencyDetails(XMLEvent event) {
        return event.isEndElement() && StaxParserUtilities.endElementMatchesStartElement(POZYCJA,event.asEndElement());
    }

    private void readPublicationDate(XMLEventReader eventReader, CurrencyDayInfoBuilder builder) throws XMLStreamException {
        builder.withPublicationDate(eventReader.getElementText());
    }
}
