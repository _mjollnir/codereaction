package pl.parser.nbp.parser;

import pl.parser.nbp.CurrencyDailyInfo;

import javax.xml.stream.XMLStreamException;
import java.io.Reader;
import java.util.Currency;
import java.util.Optional;

public interface CurrencyParser {

    Optional<CurrencyDailyInfo> parseInput(Reader reader, Currency currency);
}
