package pl.parser.nbp;

import java.math.BigDecimal;

/**
 * Try JSR 354 - interface may change.
 */
public interface CurrencyOperator {

    BigDecimal averageBuyingPrice();
    BigDecimal standardDeviationSellingPrice();
}
