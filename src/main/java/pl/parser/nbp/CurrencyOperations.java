package pl.parser.nbp;

import pl.parser.nbp.http.CompositeCurrencyResourceNameProvider;
import pl.parser.nbp.http.HttpNBPCurrencyResource;
import pl.parser.nbp.http.NBPCurrencyResourceNameProvider;
import pl.parser.nbp.parser.CurrencyParser;
import pl.parser.nbp.parser.CurrencyStaxParser;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Currency;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toList;

/**
 * Builder class constructing Currency Operator class, working on a given currency within given time frame.
 */
public class CurrencyOperations {

    CurrencyParser parser = new CurrencyStaxParser();

    private Currency currency;
    private LocalDate from;
    private LocalDate to;

    public CurrencyOperations forCurrency(Currency currency) {
        this.currency = currency;
        return this;
    }

    public CurrencyOperations withinTimeFrame(LocalDate fromDate, LocalDate toDate) {
        this.from=fromDate;
        this.to = toDate;
        return this;
    }

    public CurrencyOperator createOperator(){
        CompositeCurrencyResourceNameProvider.Builder resourceNamesProviderBuilder = new CompositeCurrencyResourceNameProvider.Builder();
        for(int year = from.getYear(); year<=to.getYear(); year++){
            resourceNamesProviderBuilder.addSupportForYear(year);
        }
        NBPCurrencyResourceNameProvider namesProvider = resourceNamesProviderBuilder.build();

        NBPCurrenciesDataSet dataSet = Stream.iterate(from, date -> date.plusDays(1))
                .limit(ChronoUnit.DAYS.between(from, to) + 1)
                .parallel()
                .map(date -> namesProvider.getCurrencyResourceNameForDate(date))
                .filter(fileName -> fileName.isPresent())
                .map(fileName -> new HttpNBPCurrencyResource(fileName.get()))
                .map(resource -> parser.parseInput(resource.getReader(), currency))
                .filter(currencyDailyData -> currencyDailyData.isPresent())
                .map(optionalDailyData -> optionalDailyData.get())
                .collect(collectingAndThen(toList(), dailyDataList -> new NBPCurrenciesDataSet(currency, dailyDataList)));


        FixedCurrencyPeriodOperator fixedCurrencyPeriodOperator = new FixedCurrencyPeriodOperator(currency, from, to,dataSet);
        return fixedCurrencyPeriodOperator;
    }
}
